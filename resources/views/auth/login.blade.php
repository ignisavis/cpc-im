<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>CPC e-GP Issue Manager</title>

    <!-- Bootstrap -->
    <link href="{!!url('/public/css/bootstrap.min.css')!!}" rel="stylesheet">
    <link href="{!!url('/public/css/custom.css')!!}" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    
	<div class="container">
		<div class="row">
			<div id="header" class="col-md-8 col-md-offset-2">
				<h3 class="text-center">Contract & Procurement Cell</h4>
			</div>
			 
			
		</div>
		<div class="clearfix blank-div"><br/><br/><br/><br/></div>
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				@if (count($errors) > 0)
					@foreach ($errors->all() as $error)
						<div class="alert alert-danger" role="alert">
						<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
						<span class="sr-only">Error:</span>
						{{ $error }}
						</div>
					@endforeach
				@endif
			
				<form class="form-horizontal" method="post" action="{!!url('/auth/login')!!}">
				 {!! csrf_field() !!}
					 
				  <div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label">Email</label>
					<div class="col-sm-10">
					  <input type="email" name="email" class="form-control" id="inputEmail3" placeholder="Email" value="{!!old('email')!!}" >
					</div>
				  </div>
				  <div class="form-group">
					<label for="inputPassword3" class="col-sm-2 control-label">Password</label>
					<div class="col-sm-10">
					  <input type="password" name="password" class="form-control" id="inputPassword3" placeholder="Password">
					</div>
				  </div>
				  <div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
					  <div class="checkbox">
						<label>
						  <input type="checkbox" name="remember"> Remember Me
						</label>
					  </div>
					</div>
				  </div>
				  <div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
					  <button type="submit" class="btn btn-default">Sign in</button>
					</div>
				  </div>
				</form>
			</div>
		</div>
	</div>
	
	
	

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
	<script>
		
	</script>
	
	
  </body>
</html>