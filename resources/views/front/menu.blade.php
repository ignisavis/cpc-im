<div class="col-md-8 col-md-offset-2">

	<ul class=" list-inline">
	  <li role="presentation"><a href="{!!url('/')!!}">Home</a></li>
	  <li role="presentation">
		  <a href="#/" class="dropdown-toggle" data-toggle="dropdown">Add</a>
		  <ul class="dropdown-menu" aria-labelledby="dropdownMenu4">
			  <li><a href="{{url('users/create')}}">User</a></li>
			  <li><a href="{{url('issues/create')}}">Issue</a></li>
			  <li><a href="{{url('clients/create')}}">Client</a></li>
		  </ul>
	  </li>
	  <li role="presentation"><a href="/">Issues</a></li>
	  <li role="presentation"><a href="/">Jobs</a></li>
	  <li role="presentation"><a href="{!!url('users')!!}">Users</a></li>
	  <li role="presentation"><a href="{!!url('/auth/logout')!!}">Logout</a></li>
	</ul>
	
</div>