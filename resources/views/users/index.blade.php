<!--
/**
 * Created by PhpStorm.
 * User: user
 * Date: 1/30/2016
 * Time: 4:11 PM
 */
-->
@extends('layouts.master')

@section('content')
    <div class="col-md-8 col-md-offset-2">
        <h4>List of Users</h4>

        <table class="table table-striped">
            @foreach ($users as $user)
                <tr>
                    <td>{{ $user->id }}</td>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                    <td>{{ $user->designation }}</td>
                    <td>{{ $user->created_at}}</td>
                    <td>
                        <a href="{!!url('users/edit/'.$user->id)!!}"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>
                    </td>
                    <td>
                        <a href="{{ $user->id }}"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
                    </td>

                </tr>
            @endforeach
        </table>




    </div>


@endsection