@extends('layouts.master')

@section('content')
    <div class="col-md-8 col-md-offset-2">
        <h4>Create a User</h4>
        @if (count($errors) > 0)
            @foreach ($errors->all() as $error)
                <div class="alert alert-danger" role="alert">
                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                    <span class="sr-only">Error:</span>
                    {{ $error }}
                </div>
            @endforeach
        @endif
        <form actioin="" method="post" id="userCreationForm">
            {!! csrf_field() !!}
            <div class="form-group">
                <label for="name">name</label>
                <input type="text"  class="form-control" id="name" name="name" placeholder="name">
            </div>
            <div class="form-group">
                <label for="email">Email address</label>
                <input type="email"  class="form-control" id="email" name="email" placeholder="Email">
            </div>
            <div class="form-group">
                <label class="control-label">Password</label>
                <div class="">
                    <input type="password" class="form-control" name="password" />
                </div>
            </div>

            <div class="form-group">
                <label class="control-label">Retype password</label>
                <div class="">
                    <input type="password" class="form-control" name="confirmPassword" />
                </div>
            </div>
            <div class="form-group">
                <label for="designation">Designation</label>
                <select id="designation" name="designation" class="form-control">
                    <option value=""></option>
                    <option value="AE">Assistant Engineer</option>
                    <option value="SDE">Sub-Divisional Engineer</option>
                    <option value="DIR">Director</option>
                    <option value="EE">Executive Engineer</option>
                    <option value="AP">Assistant Programmer</option>
                    <option value="CON">Consultant</option>

                </select>
            </div>


            <button type="submit" class="btn btn-default">Create</button>
        </form>


    </div>


@endsection